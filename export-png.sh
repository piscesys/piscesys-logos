#!/bin/bash

# Export path. DO NOT add a splash ("/") at the end!
expath=png

echo "This script export ALL SVG files in this directory to PNG files in directory $expath ."
echo 'You should have Inkscape installed before running this script.'
read -r -p 'Continue? [Y/n] ' input
case $input in
    [yY][eE][sS]|[yY])
        echo '------'
        ;;
    *)
        echo 'Quitting.'
        exit 1
        ;;
esac

havefail=false

mkdir -p $expath

for i in `ls -A | grep .svg`
do
    echo "Exporting $i..."
    inkscape --export-type=png --export-filename="$expath/${i%%.svg}.png" $i
    ret=$?
    echo "Export $i done returning $ret."
    if [ $ret != 0 ]; then
        echo "------------- $i FAILED! -------------"
        havefail=true
    else
        echo "------"
    fi
done

echo 'All export done.'
if $havefail ; then
    echo 'At least one export has failed. Please check the outputs.'
fi
