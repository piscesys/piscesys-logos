# Pisces System Logos

SVG files of the logos & slogans of Pisces System. Run export-png.sh to export them to PNG files.

## License

All Piscesys Logos has been licensed by CC-BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>. export-png.sh has been licensed by GPLv3 or later.
